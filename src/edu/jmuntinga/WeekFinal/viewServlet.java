package edu.jmuntinga.WeekFinal;

import edu.jmuntinga.WeekFinal.JSON;
import edu.jmuntinga.WeekFinal.Rental;
import edu.jmuntinga.WeekFinal.RentalB;

import javax.json.Json;
import javax.servlet.RequestDispatcher;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/")

public class viewServlet extends HttpServlet {

    RentalB t = RentalB.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    // Displayed when servlet is accessed directly
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertUser(request, response);
                    break;
                default:
                    listUser(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        RentalB t = RentalB.getInstance();


        List<Rental> p = t.getRentalList();
        String json = JSON.carToJSON(p);
        request.setAttribute("listUser", json);
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-list.jsp");
        dispatcher.forward(request, response);

    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-form.jsp");
        dispatcher.forward(request, response);
    }

    private void insertUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String books = request.getParameter("books");

        RentalB t = RentalB.getInstance();

        List<Rental> p = t.addRentalDetails(name, email, books);
        response.sendRedirect("list");
    }
}
