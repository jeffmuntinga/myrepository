package edu.jmuntinga.WeekFinal;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import bookrental.*;

import java.util.Iterator;
import java.util.List;

public class RentalB {
    SessionFactory factory = null;
    Session session = null;

    private static bookrental.RentalB single_instance = null;

    public RentalB() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static bookrental.RentalB getInstance() {
        if (single_instance == null) {
            single_instance = new bookrental.RentalB();
        }

        return single_instance;
    }


    public List<Rental> getRentalList() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from bookrental.Rental";
            List<Rental> cs = (List<Rental>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public List<Rental> addRentalDetails(String name, String email, String books) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Rental r = new Rental();
            r.setName(name);
            r.setEmail(email);
            r.setBooks(books);
            session.save(r);
            String sql = "from booksrental.Rental";
            List<Rental> cs = (List<Rental>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}