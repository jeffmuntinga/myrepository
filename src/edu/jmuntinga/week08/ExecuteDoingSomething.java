package edu.jmuntinga.week08;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteDoingSomething {

    public static void main(String[] args) {



        ExecutorService myService = Executors.newFixedThreadPool(3);
        DoingSomething ds1 = new DoingSomething("Tracie", 25, 500);
        DoingSomething ds2 = new DoingSomething("Josh", 12, 900);
        DoingSomething ds3 = new DoingSomething("Jeffrey", 2, 500);
        DoingSomething ds4 = new DoingSomething("Katie", 7, 300);
        DoingSomething ds5 = new DoingSomething("Michelle", 1, 200);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();


    }
}
