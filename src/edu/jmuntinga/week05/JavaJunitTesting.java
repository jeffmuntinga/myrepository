package edu.jmuntinga.week05;

import org.junit.Test;
import static org.junit.Assert.*;

public class JavaJunitTesting {
    public JavaJunitTesting() {
    }

    /**
     * assertEquals() to check if 40 divided by 2 is 20
     **/
    @Test
    public void test1() {
        System.out.println("\tFirst Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(40, 2);
        assertEquals(20, i);
        System.out.println("\tCompleted");
    }


    // assertTrue()
    @Test
    public void test2() {
        System.out.println("\tSecond Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(200, 10);
        assertTrue(i == 20);
        System.out.println("\tCompleted");
    }

    // assertFalse()
    @Test
    public void test3() {
        System.out.println("\tThird Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(60, 5);
        assertFalse(i == 70);
        System.out.println("\tCompleted");
    }

    // assertNotNull()
    @Test
    public void test4() {
        System.out.println("\tFourth Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(100, 5);
        assertNotNull(i);
        System.out.println("\tCompleted");
    }

    //assertNull()
    @Test
    public void test5() {
        System.out.println("\tFifth Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(0, 5);
        assertNull(i);
        System.out.println("\tCompleted");
    }

    //assertSame()
    @Test
    public void test6() {
        System.out.println("\tSixth Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(100, 5);
        int x = test.JavaJunitTesting(200, 10);
        assertSame(i, x);
        System.out.println("\tCompleted");
    }

    //assertNotSame()
    @Test
    public void test7() {
        System.out.println("\tSeventh Test Case");
        javaJunit test = new javaJunit();
        int i = test.JavaJunitTesting(100, 5);
        int x = test.JavaJunitTesting(100, 10);
        assertNotSame(i, x);
        System.out.println("\tCompleted");
    }
}
