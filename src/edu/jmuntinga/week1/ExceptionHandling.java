package edu.jmuntinga.week1;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExceptionHandling {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double firstInt;
        double secondInt;

        System.out.print("Please enter the first number and press Enter ");
        firstInt = in.nextDouble();
        System.out.print("Please enter the second number and press Enter ");
        secondInt = in.nextDouble();

        doDivision(firstInt, secondInt);
    }

    private static void doDivision(double firstInt, double secondInt) {
        Scanner in = new Scanner(System.in);
        do {
            if(secondInt==0) {
                System.out.print("It looks like you entered 0 Please enter the second number and press Enter \r");
            }
            secondInt = in.nextDouble();
            System.out.print("Thank you for entering a new number\r");
        } while (secondInt == 0);
        System.out.println("The division of the 2 numbers is ");
        System.out.println(firstInt/secondInt);
    }

}
