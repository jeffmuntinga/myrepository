package edu.jmuntinga.week1;

public record week1(String title, String director) {

    public String toString() {
        return "Title: " + title + " Director: " + director;
    }
}
