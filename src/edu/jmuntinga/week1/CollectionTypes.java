package edu.jmuntinga.week1;

import java.util.*;

public class CollectionTypes {

    public static void main(String[] args) {

        System.out.println("-- List --");
        List<String> list = new ArrayList<>();
        list.add("Hi");
        list.add("my");
        list.add("name");
        list.add("is");
        list.add("Jeffrey");
        list.add("Second time taking this class");

        for (String str : list) {
            System.out.println(str);
        }


        System.out.println("-- Set --");
        Set<String> set = new TreeSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        set.add("4");

        for (String str : set) {
            System.out.println(str);
        }


        System.out.println("-- Queue --");
        Queue<String> queue = new PriorityQueue<>();
        queue.add("Hey");
        queue.add("I am");
        queue.add("Learning");
        queue.add("Java");
        //These will be inserted due to their priority in this list
        queue.add("Hey");
        queue.add("Java");
        queue.add("I am");

        Iterator<String> iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        // By using a map I can specify where the code will be ran
        System.out.println("-- Map-- ");
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "I");
        map.put(4, "Java");
        map.put(2, "really");
        map.put(5, "!");
        map.put(3, "enjoy");
        map.put(6, "I promise!");

        for (int i = 1; i < 7; i++) {
            String result = map.get(i);
            System.out.println(result);
        }
    }
}