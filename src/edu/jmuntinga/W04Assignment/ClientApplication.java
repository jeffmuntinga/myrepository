package edu.jmuntinga.W04Assignment;

public class ClientApplication {
    private String name;
    private String emailAddress;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


    public String toString() {
        return "Name: " + name + " Email: " + emailAddress;
    }
}

