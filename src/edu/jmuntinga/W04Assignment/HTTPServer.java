package edu.jmuntinga.W04Assignment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;
import java.util.*;

public class HTTPServer {
    public static String familyToJson(ClientApplication family) {
        ObjectMapper mapper = new ObjectMapper();
        String g = "";

        try {
            g = mapper.writeValueAsString(family);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return g;
    }
    public static ClientApplication JsonToFamily(String g) {
        ObjectMapper mapper = new ObjectMapper();
        ClientApplication family = null;

        try {
            family = mapper.readValue(g, ClientApplication.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return family;
    }

    public static String getMyInfo(String family) {


        try {
            URL url = new URL(family);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "/n");
            }

            return stringBuilder.toString();


        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return "Error!";
    }

    public static Map getHTTPHeaders(String family) {
        try {

            URL url = new URL(family);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            return http.getHeaderFields();

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return null;
    }

    public static void main(String[] args) {

        System.out.println(HTTPServer.getMyInfo("https://jsonplaceholder.typicode.com/users"));

        Map<Integer, List<String>> familyMap = HTTPServer.getHTTPHeaders("https://jsonplaceholder.typicode.com/users");

        assert familyMap != null;
        for (Map.Entry<Integer, List<String>> entry : familyMap.entrySet() ) {

            System.out.println("Key= " + entry.getKey() + " Value= " + entry.getValue());
        }

        ClientApplication sibling = new ClientApplication();
        sibling.setName("Michelle Muntinga");
        sibling.setEmailAddress("metmuntinga@mail.com");

        String json = HTTPServer.familyToJson(sibling);
        System.out.println(json);

        ClientApplication brother2 = HTTPServer.JsonToFamily(json);
        System.out.println(brother2);
    }
}
